/**
 * @class       : mathjax
 * @author      : oli (oli@oli-desktop)
 * @created     : Monday Feb 20, 2023 11:37:13 GMT
 * @description : mathjax
 */

window.MathJax = {
  tex: {
    inlineMath: [["\\(", "\\)"]],
    displayMath: [["\\[", "\\]"]],
    processEscapes: true,
    processEnvironments: true
      tags: 'ams'
  },
  options: {
    ignoreHtmlClass: ".*|",
    processHtmlClass: "arithmatex"
  }
};

document$.subscribe(() => { 
  MathJax.typesetPromise()
})


