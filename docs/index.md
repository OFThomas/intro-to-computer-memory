# Welcome to MkDocs

For full documentation visit [https://ofthomas.gitlab.io/intro-to-computer-memory/](https://ofthomas.gitlab.io/intro-to-computer-memory/).

The code examples are hosted in the repository
[https://gitlab.com/OFThomas/intro-to-computer-memory](https://gitlab.com/OFThomas/intro-to-computer-memory).


# CPU

Go [here](cpu.md).

# Software

Go [here](software.md).


# GPUs

- GPUs

``` mermaid
graph LR
  A[Start] --> B{Error?};
  B -->|Yes| C[Hmm...];
  C --> D[Debug];
  D --> B;
  B ---->|No| E[Yay!];
```
