# Types of memory

We'll consider the flow of data to/from external storage: 

```mermaid
graph LR
       RAM --> Cache
    Cache --> RAM 
    
    Cache --> Registers
    Registers --> Cache

    subgraph CPU
        subgraph Operations
            Registers
        end
        Cache
    end
    subgraph Exstore [External Storage]
        HDD/SSD
    end 

    subgraph mainmem [Main Memory]
        RAM
    end

    HDD/SSD --> RAM
    RAM --> HDD/SSD

 
    
```
All of the data processing happens on the registers inside of the CPU. 

If we use the following example we can work through what we might expect to
happen in loading a value from external storage, adding a constant to it
and writing the new value back to external storage. 

We want to load the value corresponding to some memory location _x_ and add
1 to it, for this example the value at location _x_ is an char, which is a
single byte.

* Check if the value _x_ points to is in the registers 
```mermaid
flowchart

A[Start] --> B{Is x in a<br> register}
B -->|Yes| C[Add 1 to it]
B -->|No| D{Is x in <br>the Cache}
D -->|Yes| E[Load Cache to Register]
E -->|10ns| C
D -->|No| F{Is x in <br>the RAM}
F -->|Yes| G[Load RAM to Cache]
G -->|100ns| E
F -->|No| H[Load external storage <br> to RAM]
H -->|2ms| G
```

* Then write to external storage, which is the same process but in reverse.



## Hard disks / External storage

- Hard disks (metal platters) have typical latencies of 2-4ms
- Solid state flash disks have latencies of 0.25ms

## RAM 
- Typical latencies of 10 clocks which is about ~10ns for the first
  word latency 

## Cache 
In a typical (modern) x86 chip there are 3 levels of cache, the lower the number, the faster and smaller the cache level.

As an example we shall consider an intel Haswell chipset, i7-4770 3.4GHz <https://www.7-cpu.com/cpu/Haswell.html>.

### Memory

Where:

- 1 Byte is 8 bits
- 1KB = 1000 Bytes
- 1MB = 1000KB
- 1GB = 1000MB

<span style="color:red">Note there are two conventions for the number of
bits/bytes in 1KB e.g. 1000 Bytes or 1024 Bytes, here we always use powers
of 10</span>.

A typical amount of total system memory might look like:

| Device   | Memory | Speed  |
|----------|--------|--------|
| HDD      | 500GB  | Snail  |
| RAM      | 32GB   | Hare   |
| Cache L3 | 8MB    | Car    |
| Cache L2 | 256KB  | Plane  |
| Cache L1 | 32KB   | Rocket |

The smaller capacity memory typically is able to perform much faster in a PC due to different architecture choices.

## Latency

The latency is the amount of time one has to wait for a response after
trying to read or write to the store, its normally measured in clocks
(number of clock cycles of the CPU), although RAM has a a much lower clock
speed than the CPU so depending on which `clock` is referenced the actual
time in seconds may be out by a factor of 1-3.

- RAM 36 clocks + 57ns
- L3 hit 36 clocks
- L2 hit 12 clocks
- L1 hit 4 - 5 clocks

For the best intel Arria, <https://www.intel.com/content/www/us/en/docs/programmable/683711/21-2/cache-latency.html>

- L2 hit latency is <= 6 clocks (best case)
- L1 miss is <= 6 clocks (best case)
- L1 hit latency is 1 clock

Similar results for a newer chip <https://www.7-cpu.com/cpu/Ice_Lake.html>.

# Instruction timings 

Not all instructions are created equal. 

Considering 11th gen intel Tiger Lake,
<https://www.agner.org/optimize/instruction_tables.pdf>.


| Instruction | Latency |
|-------------|---------|
| FADD        | 3       |
| FSUB        | 3       |
| FMUL        | 4       |
| FDIV        | 14 - 16 |
| FSQRT       | 14 - 21 |
| FSIN, FCOS  | 60-140  |

## Vector instructions 
Nearly all of the floating point vector instructions are similar number of clocks to the non-vectorized versions.

| Instruction  | Latency |
|--------------|---------|
| VMOVUPS/D    | 3-4     |
| MOVNTPS/D    | 450     |
| ADDSS/D PS/D | 4       |
| MULSS/D PS/D | 4       |
| DIVPS        | 11      |
| VSQRTPS      | 12      |
| SQRTPD       | 15      |


# Pipelines

TODO


