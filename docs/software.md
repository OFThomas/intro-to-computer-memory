# Software

Go [here](software.md).

Now we'll give some examples about how different software structures map to
values in physical memory.

# Variables

We'll start with variables, most programs have them so it seems like a
reasonable place to start.

Types can be split into two parts, primative types which are provided by
the programming language which are normally small (in the number of bytes
they require) and relatively simple objects, such as numbers and characters
(i.e. ASCII).

```mermaid
mindmap
Types
    primative
        int 
        float
        char
    compound
        structs
        classes
```

After defining types the distinction between variables that are values or
objects (i.e. `int` or `string`), variables which are references (i.e. the
variable refers to the address of another value/object) and variables which
are pointers, pointers are a superset of reference types as pointers can
point to other variables or other references or even other pointers, or in
fact a pointer can point to nothing `null`.

```mermaid
mindmap
Calling convention
    Values
    References
    Pointers
```

Lastly we might want to have collections of variables grouped together,
such as positions of particles or people. Or a map between numbers in
different notation or a dictionary for English and German. A type which is
a collection of objects, or pointers is called a `container`.

```mermaid
mindmap
Containers
    static 
        arrays
    dynamic
        Sequence 
            lists
            queues
        Associative
            maps
            sets
```

## Basic types

In C all basic types are value types:

| Type                     | Detail                                                                       | Size (bytes) | Size (bits) |
|--------------------------|------------------------------------------------------------------------------|--------------|-------------|
| char                     | character type, normally the smallest sized type a machine can addressed     | 1            | 8           |
| int                      | signed integer type [-32,767, +32,767]                                       | 2            | 16          |
| long, long int           | signed integer type [-2,147,483,647, +2,147,483,647]                         | 4            | 32          |
| long long, long long int | signed integer type [−9,223,372,036,854,775,807, +9,223,372,036,854,775,807] | 8            | 64          |
| float                    | single precision floating point                                              | 4            | 32          |
| double                   | double precision floating point                                              | 8            | 64          |

Other languages have more basic types but these are the ones we shall
consider at the start for simplicity.

Examples in [src/basic_types]()
### C example
An example of using these types might be:
```c
#include <assert.h>
#include <stdio.h>

int main()
{
    int x = 1;
    int y = x;
    y += 2;

    // changing y does not change x as the assignment 
    // operator copies the value of x at line 13
    assert(y == x+2);

    // cast the integer result of the addition x+y to a 
    // float then add 1.5
    float z = (float)(x + y) + 1.5f;

    printf("x = %d, y = %d, z = %f \n", x, y, z);

    return 0;
}
```
which prints 
```
x = 1, y = 3, z = 5.500000
```

### Python example

The same program in python:
```python
x = 1
y = x
y = y + 2

z = (float)(x + y) + 1.5

print("x = ", x, " y = ", y, " z = ", z)
```
which prints 
```
x =  1  y =  3  z =  5.5
```

### Nim example
And in Nim
```
let x = 1
var y = x
y += 2

let z = (x + y).float + 1.5f

echo "x = ", x, " y = ", y, " z = ", z
```
which prints
```
x = 1 y = 3 z = 5.5
```

### Java example
```java
public class basic_types
{
    public static void main(String[] args)
    {
        int x = 1;
        int y = x;
        y += 2;

        float z = (float)(x + y) + 1.5f;
        System.out.printf("x = %d, y = %d, z = %f \n", x, y, z);
    }
}
```
which prints 
```
x = 1, y = 3, z = 5.500000
```

## Variable types

There are three types of variables:

1. Value types 
2. Reference types
3. Pointers  

## Value types

Value types are always passed **by value**, this means they are either
moved or copied.

When using the assignment operator, = the value is copied from the right
hand side (rhs) to the left hand side (lhs) leaving the original variable
**unchanged**.

## Pointers

A pointer is a value (lol) that points to a memory location which itself
may contain a value or another pointer that then points to a different
location which can contain a value or another pointer and so on. 

## Reference types

Reference types do **not** copy or move variables, when a variable is
passed by reference it means that changing the new value **will change** the
original variable! 

References and pointers have very similar semantics however a reference
must always point to a valid object/value, whereas a pointer can be null. 

C does not have reference types, everything is passed by value however
using pointers you can achieve similar results to passing by reference. 

### Performance concerns

Examples in [src/values_references_pointers]()

### C++ example
```c++
#include <iostream>

int value_incr(int z)
{
    z += 1;
    return z;
}

int ref_incr(int & z)
{
    z += 1;
    return z;
}

int ptr_incr(int * z)
{
    *z += 1;
    return *z;
}

int main()
{

    {
        std::cout << "By value" << std::endl;
        int x = 1;
        std::cout << "x before = " << x << std::endl;
        int y = value_incr(x);
        std::cout << "x after = " << x << ", y = " << y << std::endl;
    }

    {
        std::cout << "By reference" << std::endl;
        int x = 1;
        std::cout << "x before = " << x << std::endl;
        int y = ref_incr(x);
        std::cout << "x after = " << x << ", y = " << y << std::endl;
    }

    {
        std::cout << "By pointer" << std::endl;
        int x = 1;
        std::cout << "x before = " << x << std::endl;
        int y = ptr_incr(&x);
        std::cout << "x after = " << x << ", y = " << y << std::endl;
    }
    return 0;
}
```
which prints
```c
By value
x before = 1
x after = 1, y = 2
By reference
x before = 1
x after = 2, y = 2
By pointer
x before = 1
x after = 2, y = 2
```

The first function `value_incr` **does not** change the value of `x` outside
the scope of the function, as the value is copied into the local variable
`z` which is then incremented and the value of `z` is returned.

The second function `ref_incr` **does** change the value of `x` outside the
scope of the function and `z` binds to the reference of `x`.

The third function `ptr_incr` **does** change the value of `x` outside the
scope of the function, in this example it behaves the same as passing `x`
by reference to `ref_incr`. 

One of the nice things about C++ is the choice of choosing which calling
convention you want to use, you can pass by value, by reference or pass
pointers by value (you can also pass pointers by reference, but why would
you...).

Pass by value and pass by reference both have the same calling syntax, you
just pass the variable _x_. The function definitions are different though,
and `&` means a reference type, just as `*` means a pointer. When passing a
variable to a function expecting a pointer you can get the address of a
variable using `&`. In the function `ptr_incr` you have to dereference the
pointer to use the value, the `*` operator deferences a pointer to return
the value it points to.

### Nim example
```
proc value_incr(z: int): int =
  # z += 1 # is a compile error 
  return z + 1

proc ref_incr(z: var int): int =
  z += 1
  return z

proc ptr_incr(z: ptr int): int =
  z[] += 1
  return z[]

when isMainModule:

  block value:
    echo "By value"
    var x = 1
    echo "x before ", x
    let y = value_incr(x)
    echo "x after = ", x, ", y = ", y

  block reference:
    echo "By ref"
    var x = 1
    echo "x before ", x
    let y = ref_incr(x)
    echo "x after = ", x, ", y = ", y

  block point:
    echo "By pointer"
    var x = 1
    echo "x before ", x
    let y = ptr_incr(x.addr)
    echo "x after = ", x, ", y = ", y
```
which prints 
```c
By value
x before 1
x after = 1, y = 2
By ref
x before 1
x after = 2, y = 2
By pointer
x before 1
x after = 2, y = 2
```

The Nim example is basically the same as the C++ example however Nim does
not allow you to modify variables when passing by values, whereas C++
creates a local copy which you can modify freely. Passing by reference uses
the `var` keyword in front of the type instead of `&`. Pointer types are
qualified with `ptr` instead of `*`, addresses are taken with the `addr` 
procedure and pointers are dereferenced with the `[]` operator instead of `*`.

### Python

Python is a mess, it only has pass by value semantics, apart from mutable
types which are always passed by reference. It is often described as pass
by assignment.

```python
def value_incr(z):
    z += 1
    return z

if __name__ == "__main__":
    x = 1
    print("x before ", x)
    y = value_incr(x)
    print("x after = ", x, ", y = ", y)
```
which prints 
```
x before  1
x after =  1 , y =  2
```

Which is the same as C++ pass by value, as `z` shadows the value of `x` but
not the same as the nim pass by value which gives a compile error on trying
to assign to a constant function argument.

# Types 

We introduced the primative types above, `char`, `int`, `float` however
there are many more types one can build up in a program.

There are two main uses for types 
1. Aliasing long compound types to shorter, easier to follow types
2. Type safety, a big win for compiled languages!

## Type aliasing

In C/C++ the `typedef` keyword allows you to define a type using types that
have already been defined,
```c
typdef char int8_t;
typedef long long int bigint;
```

# Objects
An object is an instance of a type. Consider declaring an integer `x`,
then the type of `x` is `int` and the object is `x`. The compiler uses
information and at runtime the user uses the values of the objects exisitng
within a program.

```c
int x;
char y = '0';
float z = 10.0f;
```
Here `x` is an object of type `int` and it's value is unitialized, `y` is
an object of type `char` with a value of `'0'` and `z` is an object of type
`float` with a value of `1.5`.

# Arrays 

An array is a set, list or vector of varaibles of the same type, in reality
it is a collection of bytes which are contiguous in memory (meaning next to
each other).

```mermaid
graph 

subgraph Array of Bytes
a[Byte0] --> b[Byte1]
b[Byte1] --> c[Byte2]
c[Byte2] --> d[Byte3]
d[Byte3] --> e[Byte4]
e[Byte4] --> f[Byte5]
end
```

We can have arrays of more complex (larger) types, for example some type T,
which might be a float or a double, an array with 4 elements of type T
would look like:

```mermaid
graph 

subgraph Array of Type T
    T0 --> T1
    T1 --> T2
    T2 --> T3
end 
```
Where each T is infact some number of bytes itself 

```mermaid
graph 

subgraph Array of Type T
subgraph T0
    t0 --> t1
    t1 --> t2
    t2 --> t3
end 
    t3 --> b0
subgraph T1
    b0[t0] --> b1[t1]
    b1[t1] --> b2[t2]
    b2[t2] --> b3[t3]
end
end
```
where in this diagram the lowercase t values correspond to individual
bytes, here our type T is 4 bytes i.e. a float or a int32.

In fact `string` types in most languages are actually arrays of `char`
types, c-style strings are null-terminated which means they are arrays of
`char`s with the last element of the array being the `'\0'` `char`.


## String types

In c
```c
char * string1;
char string2[];
char string3[10];
```

In C++ 
```c++
std::string string1;
```

In Nim
```
let string1: string
```

## Using arrays

### In C
```c
#include <stdio.h>

int main()
{
    // fixed sized array
    {
        int num = 4;
        int myints[num];

        for(int i = 0; i < num; i++)
            printf("%d, ", myints[i]);

        printf("\n");
    }
    // unsized array
    {
        int myints[] = {0, 1, 2, 3};
        int num = sizeof(myints) / sizeof(myints[0]);

        for(int i = 0; i < num; i++)
            printf("%d, ", myints[i]);

        printf("\n");
    }
    return 0;
}
```
which prints 
```
1155407936, 21951, 49346652, 32528, 
0, 1, 2, 3, 
```
as the first array is unitialized, i.e. the values are random junk that was
previously set in the memory location that the array was given.

### In C++



# Structs

A `struct` is type which is a collection of types, they can be multiple of
the same types or many completely different types, or just a single type.

### In C
```c
#include <stdio.h>

struct Vec3
{
    float x;
    float y;
};

struct Cat
{
    int age;
    float height;
    char * name;
};

int main() 
{
    {
        struct Vec3 vec;
        vec.x = 0.0f;
        vec.y = 1.0f;

        struct Cat cat;
        cat.age = 1;
        cat.height = 25.0;
        cat.name = "Mr Bill";
    }

    // or with compound literals >= c99
    {
        struct Vec3 vec = (struct Vec3){.x = 0.0f, .y = 1.0f};
        struct Cat cat = (struct Cat){.age = 1, .height = 25.0, .name = "Mr Bill"};
    }

    return 0;
}
```
### In C++
```c++
#include <iostream>
#include <string>

struct Vec3
{
    float x;
    float y;
};

struct Cat
{
    int age;
    float height;
    std::string name;
};

int main()
{
    {
        Vec3 vec;
        vec.x = 0.0f;
        vec.y = 1.0f;

        Cat cat;
        cat.age = 1;
        cat.height = 25.0;
        cat.name = "Mr Bill";
    }

    // Or constructor style 
    {
        Vec3 vec = {0.0f, 1.0f};
        Cat cat = {1, 25.0, "Mr Bill"};
    }
    return 0;
}
```

### In Nim
In Nim structs are actually called `type`:

```
type Vec3 = object
  x: float
  y: float

type Cat = object
  age: int
  height: float
  name: string

when isMainModule:

  block cstyle:
    var vec: Vec3
    vec.x = 0.0
    vec.y = 1.0

    var cat: Cat
    cat.age = 1
    cat.height = 25.0
    cat.name = "Mr Bill"

    echo vec
    echo cat

  block:
    let vec = Vec3(x: 0.0, y: 1.0)
    let cat = Cat(age: 1, height: 25.0, name: "Mr Bill")

    echo vec
    echo cat
```
Which prints 
```
(x: 0.0, y: 1.0)
(age: 1, height: 25.0, name: "Mr Bill")
(x: 0.0, y: 1.0)
(age: 1, height: 25.0, name: "Mr Bill")
```

### In python

Python doesn't really have an equivalent to c-style structs, I think the
closest is a dataclass from python3.7
<https://docs.python.org/3/library/dataclasses.html>

See <https://stackoverflow.com/a/45426493>

```python
from dataclasses import dataclass

@dataclass
class Vec3:
    x: float
    y: float

@dataclass 
class Cat:
    age: int
    height: float
    name: str

if __name__ == "__main__":

    vec = Vec3(1.5, 2.5)
    cat = Cat(1, 25.0, "Mr Bill")

    print(vec)
    print(cat)
```
which prints 
```
Vec3(x=1.5, y=2.5)
Cat(age=1, height=25.0, name='Mr Bill')
```

# Classes 

This is where things get spicy.

# Alignment / padding
    - Arrays
    - Structs 
# Static / dynamic allocation
    - Stack memory
    - Heap memory 

# Programming structures 

We'll start with `C` structs, which stands for structures


