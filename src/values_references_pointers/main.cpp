/**
 * @author      : oli (oli@oli-desktop)
 * @file        : main
 * @created     : Monday Feb 20, 2023 21:21:08 GMT
 */

#include <iostream>

int value_incr(int z)
{
    z += 1;
    return z;
}

int ref_incr(int & z)
{
    z += 1;
    return z;
}

int ptr_incr(int * z)
{
    *z += 1;
    return *z;
}

int main()
{

    {
        std::cout << "By value" << std::endl;
        int x = 1;
        std::cout << "x before = " << x << std::endl;
        int y = value_incr(x);
        std::cout << "x after = " << x << ", y = " << y << std::endl;
    }

    {
        std::cout << "By reference" << std::endl;
        int x = 1;
        std::cout << "x before = " << x << std::endl;
        int y = ref_incr(x);
        std::cout << "x after = " << x << ", y = " << y << std::endl;
    }

    {
        std::cout << "By pointer" << std::endl;
        int x = 1;
        std::cout << "x before = " << x << std::endl;
        int y = ptr_incr(&x);
        std::cout << "x after = " << x << ", y = " << y << std::endl;
    }
    return 0;
}

