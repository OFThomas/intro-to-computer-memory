def value_incr(z):
    z += 1
    return z

if __name__ == "__main__":
    x = 1
    print("x before ", x)
    y = value_incr(x)
    print("x after = ", x, ", y = ", y)
