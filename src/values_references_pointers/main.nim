proc value_incr(z: int): int =
  # z += 1 # is a compile error 
  return z + 1

proc ref_incr(z: var int): int =
  z += 1
  return z

proc ptr_incr(z: ptr int): int =
  z[] += 1
  return z[]

when isMainModule:

  block value:
    echo "By value"
    var x = 1
    echo "x before ", x
    let y = value_incr(x)
    echo "x after = ", x, ", y = ", y

  block reference:
    echo "By ref"
    var x = 1
    echo "x before ", x
    let y = ref_incr(x)
    echo "x after = ", x, ", y = ", y

  block point:
    echo "By pointer"
    var x = 1
    echo "x before ", x
    let y = ptr_incr(x.addr)
    echo "x after = ", x, ", y = ", y
