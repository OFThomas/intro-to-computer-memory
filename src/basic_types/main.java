/**
 * @author      : oli (oli@oli-desktop)
 * @file        : basic_types
 * @created     : Monday Feb 20, 2023 20:52:09 GMT
 */

public class basic_types
{
    public static void main(String[] args)
    {
        int x = 1;
        int y = x;
        y += 2;

        float z = (float)(x + y) + 1.5f;
        System.out.printf("x = %d, y = %d, z = %f \n", x, y, z);
    }
}


