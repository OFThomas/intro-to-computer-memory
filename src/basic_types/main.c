/**
 * @author      : oli (oli@oli-desktop)
 * @file        : basic_types
 * @created     : Monday Feb 20, 2023 18:37:07 GMT
 */

#include <assert.h>
#include <stdio.h>

int main()
{
    int x = 1;
    int y = x;
    y += 2;

    // changing y does not change x as the assignment 
    // operator copies the value of x at line 13
    assert(y == x+2);

    // cast the integer result of the addition x+y to a 
    // float then add 1.5
    float z = (float)(x + y) + 1.5f;

    printf("x = %d, y = %d, z = %f \n", x, y, z);

    return 0;
}
