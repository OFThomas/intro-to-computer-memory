/**
 * @author      : oli (oli@oli-desktop)
 * @file        : main
 * @created     : Tuesday Feb 21, 2023 13:50:05 GMT
 */

#include <stdio.h>

int main()
{
    // fixed sized array
    {
        int num = 4;
        int myints[num];

        for(int i = 0; i < num; i++)
            printf("%d, ", myints[i]);

        printf("\n");
    }
    // unsized array
    {
        int myints[] = {0, 1, 2, 3};
        int num = sizeof(myints) / sizeof(myints[0]);

        for(int i = 0; i < num; i++)
            printf("%d, ", myints[i]);

        printf("\n");
    }
    return 0;
}
