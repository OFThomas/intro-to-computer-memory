/**
 * @author      : oli (oli@oli-desktop)
 * @file        : main
 * @created     : Tuesday Feb 21, 2023 16:58:04 GMT
 */

#include <stdio.h>

struct Vec3
{
    float x;
    float y;
};

struct Cat
{
    int age;
    float height;
    char * name;
};

int main() 
{
    {
        struct Vec3 vec;
        vec.x = 0.0f;
        vec.y = 1.0f;

        struct Cat cat;
        cat.age = 1;
        cat.height = 25.0;
        cat.name = "Mr Bill";
    }

    // or with compound literals >= c99
    {
        struct Vec3 vec = (struct Vec3){.x = 0.0f, .y = 1.0f};
        struct Cat cat = (struct Cat){.age = 1, .height = 25.0, .name = "Mr Bill"};
    }

    return 0;
}

