from dataclasses import dataclass

@dataclass
class Vec3:
    x: float
    y: float

@dataclass 
class Cat:
    age: int
    height: float
    name: str

if __name__ == "__main__":

    vec = Vec3(1.5, 2.5)
    cat = Cat(1, 25.0, "Mr Bill")

    print(vec)
    print(cat)
