type Vec3 = object
  x: float
  y: float

type Cat = object
  age: int
  height: float
  name: string

when isMainModule:

  block cstyle:
    var vec: Vec3
    vec.x = 0.0
    vec.y = 1.0

    var cat: Cat
    cat.age = 1
    cat.height = 25.0
    cat.name = "Mr Bill"

    echo vec
    echo cat

  block:
    let vec = Vec3(x: 0.0, y: 1.0)
    let cat = Cat(age: 1, height: 25.0, name: "Mr Bill")

    echo vec
    echo cat
