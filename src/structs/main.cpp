/**
 * @author      : oli (oli@oli-desktop)
 * @file        : main
 * @created     : Tuesday Feb 21, 2023 17:02:41 GMT
 */

#include <iostream>
#include <string>

struct Vec3
{
    float x;
    float y;
};

struct Cat
{
    int age;
    float height;
    std::string name;
};

int main()
{
    {
        Vec3 vec;
        vec.x = 0.0f;
        vec.y = 1.0f;

        Cat cat;
        cat.age = 1;
        cat.height = 25.0;
        cat.name = "Mr Bill";
    }

    // Or constructor style 
    {
        Vec3 vec = {0.0f, 1.0f};
        Cat cat = {1, 25.0, "Mr Bill"};
    }
    return 0;
}

